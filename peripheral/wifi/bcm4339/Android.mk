# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

ifeq ($(findstring imx, $(soc_name)), imx)
ifneq ($(TARGET_SIMULATOR),true)

# HAL module implemenation, not prelinked, and stored in
# hw/<SENSORS_HARDWARE_MODULE_ID>.<ro.product.board>.so
include $(CLEAR_VARS)

LOCAL_CFLAGS += -Wno-unused-parameter
LOCAL_C_INCLUDES += device/generic/brillo/wifi_driver_hal/include
LOCAL_SHARED_LIBRARIES := libcutils
LOCAL_CPPFLAGS += -DLOG_TAG=\"hal_bcmdhd\"
#LOCAL_CPPFLAGS += -DBCMDHD_USE_KERNEL_MODULE
LOCAL_SRC_FILES := wifi_driver_hal_bcmdhd.cpp
LOCAL_MODULE := wifi_driver.$(soc_name)
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_TAGS := eng

include $(BUILD_SHARED_LIBRARY)

endif # !TARGET_SIMULATOR
endif
