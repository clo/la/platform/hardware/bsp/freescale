/*
 * Copyright 2015 The Android Open Source Project
 * Copyright (C) 2015 Freescale Semiconductor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <external/zlib/zlib.h>
#include "bootctl.h"

static TBootCtl g_tBootCtl;
static char *g_MiscFile = MISC_PARTITION;

static void dumpMeta()
{
    int i;
    int fd = -1;
    int ret = 0;
    uint32_t crc;

    fd = open(g_MiscFile, O_RDONLY);
    if(fd < 0) {
        printf("%s, open %s failed, fd %d, errno %d\n", __FUNCTION__, g_MiscFile, fd, errno);
        return;
    }

    lseek(fd, BOOTCTRL_OFFSET, SEEK_SET);

    ret = read(fd, &g_tBootCtl, sizeof(TBootCtl));
    if(ret != sizeof(TBootCtl)) {
        close(fd);
        printf("%s, read failed, expect %d, actual %d\n", __FUNCTION__, sizeof(TBootCtl), ret);
        return;
    }

    printf("m_RecoveryTryRemain %d, crc %u, CRC_DATA_OFFSET %d, magic %c %c %c %c\n",
        g_tBootCtl.m_RecoveryTryRemain, g_tBootCtl.m_crc, CRC_DATA_OFFSET,
        g_tBootCtl.m_magic[0], g_tBootCtl.m_magic[1],
        g_tBootCtl.m_magic[2], g_tBootCtl.m_magic[3]);

    crc = crc32(0, (uint8_t *)&g_tBootCtl + CRC_DATA_OFFSET, sizeof(g_tBootCtl) - CRC_DATA_OFFSET);
    if(crc != g_tBootCtl.m_crc) {
        printf("!!! warning, calculated crc %d, read crc %u\n", crc, g_tBootCtl.m_crc);
        close(fd);
        return;
    }

    for(i = 0; i < SLOT_NUM; i++) {
        printf("slot %d: pri %d, try %d, suc %d\n", i,
            g_tBootCtl.m_aSlotMeta[i].m_Priority,
            g_tBootCtl.m_aSlotMeta[i].m_TryRemain,
            g_tBootCtl.m_aSlotMeta[i].m_BootSuc);
    }

    close(fd);
    return;
}

static void writeMeta() {
    int ret = 0;
    int fd;
    uint32_t crc;
    int flag = O_WRONLY;

    if(strcmp(g_MiscFile, MISC_PARTITION))
        fd = open(g_MiscFile, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    else
        fd = open(g_MiscFile, O_WRONLY);

    if(fd < 0) {
        printf("%s, open %s failed, fd %d, errno %d\n", __FUNCTION__, g_MiscFile, fd, errno);
        return;
    }

    if(strcmp(g_MiscFile, MISC_PARTITION)) {
        struct bootloader_message btm;
        memset(&btm, 0, sizeof(btm));
        write(fd, &btm, sizeof(btm));
    }

    crc = crc32(0, (uint8_t *)&g_tBootCtl + CRC_DATA_OFFSET, sizeof(g_tBootCtl) - CRC_DATA_OFFSET);
    g_tBootCtl.m_crc = crc;

    lseek(fd, BOOTCTRL_OFFSET, SEEK_SET);
    ret = write(fd, &g_tBootCtl, sizeof(TBootCtl));
    if(ret != sizeof(TBootCtl)) {
        close(fd);
        printf("%s, write failed, expect %d, actual %d\n", __FUNCTION__, sizeof(TBootCtl), ret);
        return;
    }

    fsync(fd);
    close(fd);

    return;
}

int main(int argc, char *argv[])
{
    int i;
    int fd;
    int ret;
    int count = 0;
    char *pMetaFile = g_MiscFile;

    if(argc < 2){
        printf("usage: %s set aPri, aTry, aSuc, bPri, bTry, bSuc\n", argv[0]);
        printf("usage: %s get\n", argv[0]);
        return 1;
    }

    //get meta
    if((strcmp(argv[1], "get") == 0)) {
        dumpMeta();
        return 0;
    }

    //set meta
    if(argc < 8) {
        printf("usage: %s set aPri, aTry, aSuc, bPri, bTry, bSuc\n", argv[0]);
        return 1;
    }

    if(argc >= 9)
        g_MiscFile = argv[8];

    //set meta
    g_tBootCtl.m_RecoveryTryRemain = 7;
    g_tBootCtl.m_aSlotMeta[0].m_Priority = atoi(argv[2]);
    g_tBootCtl.m_aSlotMeta[0].m_TryRemain = atoi(argv[3]);
    g_tBootCtl.m_aSlotMeta[0].m_BootSuc = atoi(argv[4]);
    g_tBootCtl.m_aSlotMeta[1].m_Priority = atoi(argv[5]);
    g_tBootCtl.m_aSlotMeta[1].m_TryRemain = atoi(argv[6]);
    g_tBootCtl.m_aSlotMeta[1].m_BootSuc = atoi(argv[7]);

    g_tBootCtl.m_magic[0] = '\0';
    g_tBootCtl.m_magic[1] = 'F';
    g_tBootCtl.m_magic[2] = 'S';
    g_tBootCtl.m_magic[3] = 'L';

    g_tBootCtl.m_crc = crc32(0, (uint8_t *)&g_tBootCtl + CRC_DATA_OFFSET, sizeof(g_tBootCtl) - CRC_DATA_OFFSET);

    printf("crc %d\n", g_tBootCtl.m_crc);
    for(i = 0; i < SLOT_NUM; i++) {
        printf("slot %d: pri %d, try %d, suc %d\n", i,
            g_tBootCtl.m_aSlotMeta[i].m_Priority,
            g_tBootCtl.m_aSlotMeta[i].m_TryRemain,
            g_tBootCtl.m_aSlotMeta[i].m_BootSuc);
    }

    writeMeta();

    return 0;
}
