#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# BCM4339 WIFI Firmware
BCM4339_BT_SRC := hardware/bsp/freescale/peripheral/bluetooth/bcm4339
BCM4339_BT_FIRMWARE := vendor/bsp/freescale/peripheral/bluetooth/bcm4339/firmware
BCM4339_BT_FW_DST := system/vendor/firmware/bluetooth/bcm4339

ifeq ($(BCM4339_BT_FIRMWARE), $(wildcard $(BCM4339_BT_FIRMWARE)))
PRODUCT_COPY_FILES += \
    $(BCM4339_BT_FIRMWARE)/Type_ZP.hcd:$(BCM4339_BT_FW_DST)/BCM4335C0.ZP.hcd
else
    $(warning "Not found bcm4339 BT firmware. Please use 'tools/bdk/brunch/brunch bsp download picoimx' command to download.")
endif

PRODUCT_COPY_FILES += \
    $(BCM4339_BT_SRC)/bt_vendor.conf:system/etc/bluetooth/bt_vendor.conf

# BCM Bluetooth
BOARD_HAVE_BLUETOOTH_BCM := true
BOARD_CUSTOM_BT_CONFIG := $(BCM4339_BT_SRC)/vnd_picoimx.txt

DEVICE_PACKAGES += \
    bt_bcm_bcm4339.imx
